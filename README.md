Intro to the Unix Shell : Workshop slides
=========================================

Pat Bills, billspat@msu.edu


These are slides reated for MSU ICER workshop "Introduction to the Unix shell and shell scripting."

The slide use HTML+Javascript based on https://github.com/marcolago/flowtime.js

Code is formatted with http://prismjs.com/

The HMTL file for the slides is built with https://middlemanapp.com/   In this project, the source code for the slides are the source/_slides.haml; index.html and print.html are built from that and are in the /build folder

The slide theme is modified from the default them from the Middlemanapp app project, along with other stolen bits of css and scss

Most of the CSS is is still plain CSS but SASS may be used (and Middleman will process). 

To Use
------

*super-minimal instructions*

Fork this repo and clone to your editor

install a modern Ruby and middlemanapp via https://middlemanapp.com/ 
modify/create slides by editing source/_slides.haml
slides are in haml format but per middleman could use slim, erb or plain html

* for HTML format, see https://github.com/marcolago/flowtime.js  but..
  * all slides are in a single html file
  * sections of the slide deck use  a div with ft-section class ( .ft-section in HAML )
  * slides use a div with ft-page class
  * titles are h1 inside ft page
  * items are p below title
  * using HAML makes writing slides super easy; consider learning it
  
Change the theme of the slides by editing or replacing source/css/theme.css

Build the slides with middleman
    $ cd my_project
    $ bundle exec middleman build 

Change the look of the printed (or PDF-ed) slides by editing print.html.haml

Use the slides by copying everything in source to your webserver (or leave it on your laptop), and clicking on index.html.  if you use open the file from your computer using chrome ( and maybe others), the javascript will be disabled and the deck won't work.   You'll have to start a local webserver(or copy to another webserver)

